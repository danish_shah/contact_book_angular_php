-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 22, 2019 at 10:00 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `contact_book_angular_php`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `phone`, `email`, `image`, `active`) VALUES
(1, 'dsfsaffd', '23432', 'fsafsffa@gmail.com', NULL, 1),
(2, 'affdsdf', '3232234324', 'sfkldsfsklf@gmai', NULL, 1),
(3, 'affdsdf', '3232234324', 'sfkldsfsklf@gmail.com', NULL, 1),
(4, 'affdsdf', '3232234324', 'sfkldsfsklf@gmail.com', NULL, 1),
(5, 'affdsdf', '3232234324', 'sfkldsfsklf@gmail.com', NULL, 1),
(6, 'affdsdf', '3232234324', 'sfkldsfsklf@gmail.com', NULL, 1),
(7, 'dsffaf', '23432', 'sdfsdfafd@gmail.com', NULL, 1),
(8, 'dsfaf', '23423', '233efwrew@gmail.com', NULL, 1),
(9, 'danish shah', '343', 'danish@gmail.com', NULL, 1),
(10, 'danish shah', '343', 'danish@gmail.com', NULL, 1),
(11, 'adfdfsaf', '324', 'safdsdsff@gmail.com', NULL, 1),
(12, 'adfdfsaf', '324', 'safdsdsff@gmail.com', NULL, 1),
(13, 'adfsaf', '324', '', NULL, 1),
(14, 'adfsaf', '324', 'asdffd@gmil.vom', NULL, 1),
(15, 'adfsaf', '324', '', NULL, 1),
(16, 'adfsaf', '324', 'asdffd@gmil.vom', NULL, 1),
(17, 'adfsaf', '324', '', NULL, 1),
(18, 'afasdfff', '4324', '', NULL, 1),
(19, 'afasdfff', '4324', '', NULL, 1),
(20, 'afasdfff', '4324', '', NULL, 1),
(21, 'afasdfff', '4324', '', NULL, 1),
(22, 'afasdfff', '4324', '', NULL, 1),
(23, '324', '23', 'dasf@gmaiil.com', NULL, 1),
(24, '324', '23', '', NULL, 1),
(25, '324', '23', '', NULL, 1),
(26, '234', '2342', 'fadfa@gmail.com', NULL, 1),
(27, '234', '2342', '', NULL, 1),
(28, '234', '2342', '', NULL, 1),
(29, '234', '2342', '', NULL, 1),
(30, '234', '2342', '', NULL, 1),
(31, '234', '2342', '', NULL, 1),
(32, '234', '2342', '', NULL, 1),
(33, '234', '34223', '', NULL, 1),
(34, '234', '34223', '', NULL, 1),
(35, '234', '34223', 'aff@gnmail.com', NULL, 1),
(36, '234', '34223', '', NULL, 1),
(37, '234', '34223', '', NULL, 1),
(38, '234', '34223', '', NULL, 1),
(39, '234', '34223', '', NULL, 1),
(40, '43', '24', 'fadsff@gmail', NULL, 1),
(41, '43', '24', '', NULL, 1),
(42, '43', '24', '', NULL, 1),
(43, '43', '24', '', NULL, 1),
(44, 'dsaff', '23424', 'sdsaf@gmail.com', NULL, 1),
(45, 'dsaff', '23424', 'sdsaf@gmail.com', NULL, 1),
(46, '234', '324', 'adsfasfdfa@gmaiil.com', NULL, 1),
(47, '234', '324', 'adsfasfdfa@gmaiil.com', NULL, 1),
(48, 'asdfdff', '23432', 'sadfff@gmail.colm', NULL, 1),
(49, 'adsfsdfff', '2342', 'asdfds@gmail.com', NULL, 1),
(50, 'sdafdf', '234', 'adfsf@gmail.com', NULL, 1),
(51, 'asdf', '234', 'asfadf@gmail.com', NULL, 1),
(52, 'afadsf', '2342', 'fsafsdff@gmail.com', NULL, 1),
(53, 'asfdsfd', '234324', 'asdfsaf@gmail.com', NULL, 1),
(54, 'dsafdsaf23', '324324', 'sdfsadff@gmail.com', NULL, 1),
(55, 'LAST', '343655646556', 'daanish@gmaillast.com', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`) VALUES
(1, 'admin@angular.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
