const app = angular.module('contact_book', ['ngRoute']);

app.config(['$routeProvider',
    function ($routeProvider) {

       // let _routes = {
       //      '/':{
       //      templateUrl: 'contact_list.html',
       //      controller: 'contactController'
       //      },
       //      '/add_contact':{
            
       //      templateUrl: 'add_contact.html',
       //      controller: 'contactController'
       //      },
       //      '/edit_contact/:id':{
       //      templateUrl: 'add_contact.html',
       //      controller: 'contactController'
       //      },
            
       //   };

       //    $.each(_routes,function(path,routeOptions){
       //       $routeProvider.when(path,routeOptions)  
       //    })

       let _routes = [
       
       {route:'/',url:'contact_list.html',controller:'contactController'},
       {route:'/add_contact',url:'add_contact.html',controller:'contactController'},
       {route:'/edit_contact/:id',url:'add_contact.html',controller:'contactController'}
       
       ];


       _routes.forEach(function(value,key)  {

        $routeProvider.when(value.route,{
          templateUrl:value.url,
          controller:value.controller
        })

       });


        // $routeProvider.
        //     when('/', {
        //         templateUrl: 'contact_list.html',
        //         controller: 'contactController'
        //     })
        //     .when('/add_contact', {
        //         templateUrl: 'add_contact.html',
        //         controller: 'contactController'
        //     })
        //     .when('/edit_contact/:id', {
        //         templateUrl: 'add_contact.html',
        //         controller: 'contactController'
        //     })


    }]);

