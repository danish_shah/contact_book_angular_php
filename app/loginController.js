const login_app = angular.module('login_app', []);
login_app.controller('loginController', ($scope, $http) => {

	const api_url = 'api';

	$scope.auth = () => {

		if ($scope.login !== undefined && Object.keys($scope.login).length == 2) {

			$http({
				method: 'POST',
				url: `${api_url}/auth.php`,
				data: $scope.login
			}).then((response) => {

				if (response.data.result === 'success') {
					$scope.login_error = false;
					location.replace('contacts.html');
				} else {
					$scope.login_error = true;
				}

			}, (error) => {
				console.log(response.statusText);
			})
		}
		// auth function end
	}

	// controller function end
})
