app.controller('contactController', function ($scope, $http, $location, $timeout) {
	// document.querySelector('#dataTable').dataTable();

	const api_url = 'api';
	let edit_mode = false;
	let edit_id = 0;

	$scope.saveResponse = { _class: '', msg: '' }

	$scope.deleteContact = (_this) => {
					
		if (confirm('Are you sure you want to delete?') === true) {

			$http({
				url: `${api_url}/delete_contact.php`,
				method: 'POST',
				data: { id: _this.contact.id }

			}).then((response) => {

				if (response.data.result == 'success') {

					$scope.saveResponse._class = 'alert-success';
					$scope.saveResponse.msg = response.data.msg;

					$(`tbody > tr:eq(${_this.$index})`).remove();
						
				} else {

					$scope.saveResponse._class = 'alert-danger';
					$scope.saveResponse.msg = response.data.msg;

				}


			}, (error) => {
				$scope.saveResponse._class = 'alert-danger';
				$scope.saveResponse.msg = 'Error deleting contact';
			})

		}

	};

	$scope.editMode = () => {
		let url = $location.path().split('/');

		if (url[1] == 'edit_contact') {

			edit_mode = true;
			edit_id = url[2];

			$http({
				url: `${api_url}/fetch_contact.php`,
				method: 'POST',
				data: { id: edit_id }
			}).then((response) => {

				if (response.data.result == 'success') {
					$scope.contact = response.data.contact;
					// $.each(response.data.contact, function (key, value) {
					// 	$(`input[name=${key}]`).val(value);
					// })
				}

			})
		}
	}

	$scope.contactRepeatDone = () => {
		$('#dataTable').dataTable();
	}


	$scope.getContacts = () => {

		$http({
			method: 'POST',
			url: `${api_url}/get_contacts.php`,
		}).then((response) => {

			$scope.contacts = response.data.contacts;

		}, (error) => {

		})

	}

	$scope.saveContact = (validForm) => {


		if (validForm) {
			if (edit_mode) {
				api_file = 'edit_contact.php';
				$scope.contact.id = edit_id;
			} else {
				api_file = 'add_contact.php';
			}


			let formdata = new FormData();

			let file = document.getElementById('file');

			formdata.append('file', file.files[0]);

			$.each($scope.contact, function (key, value) {
				formdata.append(key, value);
			})

			$http({
				method: 'POST',
				url: `${api_url}/${api_file}`,
				data: formdata,
				headers: { 'Content-Type': undefined },
			}).then((response) => {


				if (response.data.result === 'success') {

					$scope.saveResponse._class = 'alert-success';
					$scope.saveResponse.msg = `${response.data.msg}. Redirecting...`;

					$timeout(() => {
						$location.path('/');
					}, 1000)

				} else {
					$scope.saveResponse._class = 'alert-danger';
					$scope.saveResponse.msg = response.data.msg
				}

			}, (error) => {

				$scope.saveResponse._class = 'alert-danger';
				$scope.saveResponse.msg = 'Error adding/updating contact';

			})

		}

	}


})