<?php
require 'includes/db.php';
require 'includes/project_helpers.php';
require 'includes/constants.php';

$post = json_decode(file_get_contents("php://input"));

$query = "update ".TABLE_CONTACT." set active=0 where id=$post->id";


if($conn->query($query)===true){
	echo json_encode(['result'=>'success','msg'=>"Contact has been deleted successfully."]);
} else {
	echo json_encode(['result'=>'failure','msg'=>"Errror deleting contact."]);
}
