<?php
require 'includes/db.php';
require 'includes/project_helpers.php';

if(!auth()){
	echo json_encode(['result'=>'failure','msg'=>'Invalid user']);
	exit;
}

$query = "select * from contacts where active=1 order by id DESC";

$result = $conn->query($query);

if($result->num_rows){

	$data = [];

	while($row = $result->fetch_assoc() ) {
		
		$data [] = $row;

	}

	echo json_encode(['result'=>'success','contacts'=>$data]);
} else {
	echo json_encode(['result'=>'failure','msg'=>'Error in fetching records']);
}

